//Variable en donde se guarda la informacion de los discos, imagen, nombre y años.
let discos = [["img/discos/00.jpg","Tu madre es una foca","2002"],["img/discos/01.jpg","Música para enfermos","2004"],["img/discos/02.jpg","Un tipo cualquiera","2006"],["img/discos/03.jpg","T.O.T.E.","2008"],["img/discos/04.jpg","El lado oscuro de Ghandi","2010"],["img/discos/05.jpg","Héroe","2012"],["img/discos/06.jpg","El tratamiento Regio","2013"],["img/discos/07.jpg","78","2015"],["img/discos/08.jpg","Lebron","2018"]];

//Variable donde se guardan todos los botones de la barra de navegacion.
let enlaces = document.querySelectorAll(".enlace");

//Variable donde se guarda el contenedor donde se van a mostrar los discos.
let contenedor = document.querySelector(".contenedor");

//Ciclo(no lo vimos todavia XD) donde se repite la accion por cada disco en la variable "discos", y los coloca dentro del "contenedor" para que se vena en la pagina.
for(i=0;i<discos.length;i++)
	{
		contenedor.innerHTML+="<div><img class='lp' src="+discos[i][0]+"><p><b>"+discos[i][1]+"</b> - "+discos[i][2]+"</p></div>"
	}


//Funcion (tampoco lo vimos todavia XD) donde a cada click de algun boton de la barra, este le asigna el estilo "select", para saber que el usuario sepa en que seccion está
const cambiar=(pos)=>
	{
		for(i=0; i<enlaces.length; i++)
			{
				enlaces[i].classList.remove("select");
			}

		enlaces[pos].classList.add("select");
	}

//Funcion que al precionar en los botones de los lados del contendor de los discos, este provoque que los discos se muevan.
const deslizar=(cantidad)=>
	{
		let conte = document.querySelector(".contenedor");
		conte.scrollBy(cantidad, 0);
	}

